﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Text;

namespace convert
{
    public class convert
    {
        public static object defaultFunc(string t)
        {
            if (t == "int")
            {
                return 0;
            }
            else if (t == "bool")
            {
                return false;
            }
            else
            {
                return null;
            }
        }

        public static List<object> convertDBsetToDTO<T>(DataSet dataset)
        {
            var isObject = false;
            List<object> list = new List<object>();
            foreach (var a in System.Attribute.GetCustomAttributes(typeof(T)))
            {
                if (a.GetType() == typeof(DTO.DTOAttribute))//it means that it is an Object
                {
                    isObject = true;
                }
            }
            if (isObject == true)
            {
                foreach (DataRow row in dataset.Tables[0].Rows)
                {
                    T Instance = Activator.CreateInstance<T>();
                    var properties = Instance.GetType().GetProperties();
                    foreach (var item in properties)
                    {
                        if (item.GetCustomAttribute<NotInSqlAttribute>() == null
                            || item.GetCustomAttribute<IdentityAttribute>() == null)
                        {
                            try
                            {
                                if (row[item.Name] != null)
                                {
                                    item.SetValue(Instance, row[item.Name]);
                                }
                                else
                                {
                                    ///if the ds is not all initilize.
                                    string t1 = row[item.Name].GetType().Name;
                                    var def = defaultFunc(t1);
                                    item.SetValue(Instance, def);
                                }
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("error convert" + e.Message);
                            }
                        }
                    }
                    list.Add(Instance);
                }
            }
            else
            {
                foreach (DataTable table in dataset.Tables)
                {
                    foreach (DataRow row in table.Rows)
                    {
                        foreach (object item in row.ItemArray)
                        {
                            list.Add(item);
                        }
                    }
                }

            }
            return list;
        }


        public static List<SqlParameter> convertDTOToSqlParameters<T>(T item, string name = null)
        {
            var isObject = false;
            List<SqlParameter> l = new List<SqlParameter>();
            //foreach (var a in Attribute.GetCustomAttributes(typeof(T)))
            //{
            //    //bool hasDtoAttribute = typeof(T).GetCustomAttributes(typeof(DTO.DTOAttribute), true).Any();
            //    if (a.GetType() == typeof(DTO.DTOAttribute))
            //    {
            //        isObject = true;
            //    }
            //}
            //if (isObject == true)
            if (name == null)
            {
                PropertyInfo[] properties = typeof(T).GetProperties();
                foreach (PropertyInfo property in properties)
                {
                    ////var p = property.GetCustomAttribute(typeof(IdentityAttribute), true).ToString();
                    if ((property.GetCustomAttribute<IdentityAttribute>() == null))
                    {
                        l.Add(new SqlParameter("@" + property.Name, property.GetValue(item)));
                    }
                }
            }
            else
            {
                l.Add(new SqlParameter("@" + name, item));
            }
            return l;
        }
    }
}


