﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    [DTO]
   public class ComponentDTO
    {
        public int id { get; set; }
        public string name { get; set; }

        public double latitude { get; set; }
        public double longitude { get; set; }
        public string showName { get; set; }

    }
}
