﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    [DTO]
    public class ResponsibleDTO
    {
        public int id { get; set; }
        public int  role { get; set; }
        public string name { get; set; }
        public string responsiblePassword { get; set; }
    }
}

  