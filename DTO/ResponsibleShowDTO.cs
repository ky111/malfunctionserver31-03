﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    [DTO]
    public class ResponsibleShowDTO
    {
        public string id { get; set; }
        public string name { get; set; }
        public string role { get; set; }
    }
}
