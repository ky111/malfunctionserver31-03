﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    [DTO]
    public class LayerDTO
    {
        [Identity]
        public int id { get; set; }
        public string name { get; set; }

        public string nameConvention { get; set; }
        public string url { get; set; }
        public int zoom { get; set; }
    }
}
