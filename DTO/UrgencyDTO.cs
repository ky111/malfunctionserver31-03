﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    [DTO]
    class UrgencyDTO
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
