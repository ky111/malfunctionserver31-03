﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    [DTO]
    public class MafunctionDTO
    {
        [IdentityAttribute]//post
        public int componentId { get; set; }
        public int malfunctionStatusId { get; set; }
        public string malfunctionDescription { get; set; }
        public int? urgencyId { get; set; }

    }
}
