﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    [DTO]
    public class HirarchiaDTO
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
