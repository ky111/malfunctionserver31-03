﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    [DTO]
    public class MalfunctionByComponentId
    {
        [Identity]
        public int id { get; set; }
        public int componentId { get; set; }
        public string malfunctionDescription { get; set; }
        public string componentName { get; set; }
       
    }
}
