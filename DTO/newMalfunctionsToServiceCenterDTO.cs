﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    
        [DTO]
        public class newMalfunctionsToServiceCenterDTO
        {
            [Identity]
            public int malfunctionDTO_id { get; set; }
            public string malfunctionStatusDTO_name { get; set; }
            public string componentDTO_name { get; set; }
            public string malfunctionDTO_description { get; set; }
            public string urgencyDTO_name { get; set; }
        }
}
