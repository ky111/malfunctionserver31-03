﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    [DTO]
    public class AddMalfunctionDTO
    {
        [Identity]
        public int id { get; set; }
        public int componentId { get; set; }
        public string malfunctionDescription { get; set; }
        public int urgencyId { get; set; }
        public string responsibleId { get; set; }
    }
}
