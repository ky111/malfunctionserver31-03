﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    [DTO]
    public class LogMalfunctionStatusDTO
    {
        [Identity]
        public int id { get; set; }
        public string name { get; set; }
    }
}
