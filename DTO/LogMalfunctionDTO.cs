﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    [DTO]
    public class LogMalfunctionDTO
    {
        [Identity]
        public int id { get; set; }
        public MafunctionDTO malfunction { get; set; }
        public ResponsibleDTO  responsible { get; set; }
        public LogMalfunctionStatusDTO  logMalfunctionStatus { get; set; }
        public string statusDescription { get; set; }
    }
}
