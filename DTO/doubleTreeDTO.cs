﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    [DTO]
   public  class doubleTreeDTO
    {
        public List<componentNode> nodes { get; set; } = new List<componentNode>();
        public List<ComponentDTO> components { get; set; } = new List<ComponentDTO>();
    }
}
