﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    [DTO]
    public class AddLogMalfunctionDTO
    {
        [Identity]
        public int id { get; set; }
        public int malfunctionId { get; set; }
        public string responsibleId { get; set; }
        public int logMalfunctionStatusId { get; set; }
        public string statusDescription { get; set; }
    }
}
