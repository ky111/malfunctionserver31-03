﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    [DTO]
    public class ViewLogDTO
    {
        [Identity]
        public int id { get; set; }
        public string ComponentName { get; set; }
        public string ResponsibleId { get; set; }
        public string ResponsibleName { get; set; }
        public string LogMalfunctionStatusName { get; set; }
        public string statusDescription { get; set; }
        public int malfunctionId { get; set; }
    }
}