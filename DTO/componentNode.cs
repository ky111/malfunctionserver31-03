﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class componentNode
    {
        public string name { get; set; }
        public string showName { get; set; }
        public List<componentNode>  children { get; set; }
        public componentNode()
        {
            children = new List<componentNode>();
        }
    }
}
