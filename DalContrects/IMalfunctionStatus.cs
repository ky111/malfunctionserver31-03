﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DalContracts
{
    public interface IMalfunctionStatus
    {
        DataSet getMalfunctionStatus(string sqlConnectionString);
    }
}
