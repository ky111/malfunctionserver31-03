﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DalContracts
{
    public interface ILogMalfunctionStatus
    {
        DataSet getLogMalfunctionStatus(string sqlConnectionString);
    }
}
