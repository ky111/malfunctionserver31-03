﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DalContracts
{
    public interface ILogMalfunction
    {
        DataSet getLogmalfunctions(string sqlConnectionString);
        DataSet addLogMalfunctionToDB(List<SqlParameter> listParameters, string sqlConnectionString);
    }
}
