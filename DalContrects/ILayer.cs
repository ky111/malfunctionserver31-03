﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlTypes;
using System.Linq.Expressions;
using System.Data.Common;

using System.Data.SqlClient;


namespace DalContracts
{
    public interface ILayer
    {
        DataSet getLayers(string con);
    }
}
