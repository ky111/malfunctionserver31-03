﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlTypes;
using System.Linq.Expressions;
using System.Data.Common;

using System.Data.SqlClient;

namespace DalContracts
{
  public interface IComponent
    {
        DataSet addComponent(SqlParameter sqlParameter,string con);
        DataSet getComponent(SqlParameter sqlParameter,string con);
        DataSet getComponentByNamingConvention(List<SqlParameter> stringParams, string con);
        DataSet getAllComponents(string con);


    }
}
