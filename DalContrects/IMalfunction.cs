﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlTypes;
using System.Linq.Expressions;
using System.Data.Common;
using System.Data.SqlClient;


namespace DalContracts
{
    public interface IMalfunction
    {
        DataSet addMalfunction(List<SqlParameter> SqlParameterList, string sqlConnectionString);
        DataSet getMalfunctionByComponentId(List<SqlParameter>
            SqlParameterList, string sqlConnectionString);
        DataSet getNewMalfuctions(string sqlConnectionString);
        DataSet getMalfunctions(string sqlConnectionString);
    }
}
