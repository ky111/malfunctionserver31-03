﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DalContracts
{
    public class ImplementAttribute:Attribute
    {
        public Type interfaceName { get; set; }
        public ImplementAttribute(Type name)
        {
            interfaceName = name;
        }
    }
}
