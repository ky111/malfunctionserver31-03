﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DalContracts
{
   public interface IRole
    {
        DataSet getRoles(string sqlConnectionString);
    }
}
