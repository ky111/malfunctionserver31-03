﻿//using System;
//using System.Collections.Generic;
//using System.Text;

//namespace DalContracts
//{
//    class IUser
//    {
//    }
//}

using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DalContracts
{
    [DTO]
    
    public interface IUser
    {
        DataSet checkUser(List<SqlParameter> listParameters, string sqlConnectionString);
        DataSet insertUser(List<SqlParameter> listParameters, string sqlConnectionString);
        DataSet getResponsible(string sqlConnectionString);
    }
}

