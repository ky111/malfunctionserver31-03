﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DalContracts
{
    public interface IHirarchia
    {
        DataSet getHirarchia(string sqlConnectionString);
    }
}
