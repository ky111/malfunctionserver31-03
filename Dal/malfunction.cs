﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using DalContracts;


namespace Dal
{
    [ImplementAttribute(typeof( IMalfunction))]
    class malfunction : IMalfunction
    {
        public DataSet addMalfunction(List<SqlParameter> SqlParameterList, string sqlConnectionString)
        {

            using (SqlConnection con= new SqlConnection(sqlConnectionString))
            {
                SqlCommand cmd = new SqlCommand("addMalfunction", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddRange(SqlParameterList.ToArray());
                DataSet ds = new DataSet();
                try
                {
                    con.Open();
                    SqlDataAdapter ad = new SqlDataAdapter();
                    ad.SelectCommand = cmd;
                    ad.Fill(ds);
                }
                catch (Exception e)
                {
                    Console.WriteLine("cant open connection" + e.Message);
                    throw;
                }
                finally {

                    con.Close();
                }
                return ds;
            }

        }
        public DataSet getMalfunctionByComponentId(List<SqlParameter> SqlParameterList, string sqlConnectionString)
        {

            using (SqlConnection con = new SqlConnection(sqlConnectionString))
            {
                SqlCommand cmd = new SqlCommand("getMalfunctionByComponentId", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddRange(SqlParameterList.ToArray());
                DataSet ds = new DataSet();
                try
                {
                    con.Open();
                    SqlDataAdapter ad = new SqlDataAdapter();
                    ad.SelectCommand = cmd;
                    ad.Fill(ds);
                }
                catch (Exception e)
                {
                    Console.WriteLine("cant open connection" + e.Message);
                    throw;
                }
                finally
                {

                    con.Close();
                }
                return ds;
            }

        }

        public DataSet getMalfunctions(string sqlConnectionString)
        {
           using (SqlConnection con = new SqlConnection(sqlConnectionString))
            {
                SqlCommand cmd = new SqlCommand("getMalfunctions", con);
                cmd.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                try
                {
                    con.Open();
                    SqlDataAdapter ad = new SqlDataAdapter();//חוזר נתונים
                    ad.SelectCommand = cmd;
                    ad.Fill(ds);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    con.Close();
                }
                return ds;
            }
        }

        public DataSet getNewMalfuctions(string sqlConnectionString)
        {
            using (SqlConnection con = new SqlConnection(sqlConnectionString))
            {
                SqlCommand cmd = new SqlCommand("getNewMalfunctions", con);
                cmd.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                try
                {
                    con.Open();
                    SqlDataAdapter ad = new SqlDataAdapter();
                    ad.SelectCommand = cmd;
                    ad.Fill(ds);
                }
                catch (Exception e)
                {
                    Console.WriteLine("cant open connection" + e.Message);
                    throw;
                }
                finally
                {
                    con.Close();
                }
                return ds;
            }
        }
    }
}







