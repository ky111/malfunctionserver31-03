﻿using DalContracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Dal
{
    [Implement(typeof(ILogMalfunction))]
    class logMalfunction:DalContracts.ILogMalfunction
    {
        public DataSet getLogmalfunctions(string sqlConnectionString)
        {

            using (SqlConnection con = new SqlConnection(sqlConnectionString))
            {
                SqlCommand cmd = new SqlCommand("getLogMalfunction", con);
                cmd.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                try
                {
                    con.Open();
                    SqlDataAdapter ad = new SqlDataAdapter();
                    ad.SelectCommand = cmd;
                    ad.Fill(ds);
                }
                catch (Exception e)
                {
                    Console.WriteLine("cant open connection" + e.Message);
                    throw;
                }
                finally
                {

                    con.Close();
                }
                return ds;
            }

        }

        public DataSet addLogMalfunctionToDB(List<SqlParameter> listParameters, string sqlConnectionString)
        {
            using (SqlConnection con = new SqlConnection(sqlConnectionString))
            {
                SqlCommand cmd = new SqlCommand("addLogMalfunction", con);
                cmd.Parameters.AddRange(listParameters.ToArray());
                cmd.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                try
                {
                    con.Open();
                    SqlDataAdapter ad = new SqlDataAdapter();
                    ad.SelectCommand = cmd;
                    ad.Fill(ds);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    con.Close();
                }
                return ds;
            }
        }

    }
}
