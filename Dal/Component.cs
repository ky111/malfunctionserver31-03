﻿using DalContracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;

namespace Dal
{

    [Implement(typeof(IComponent))]

    class Component : IComponent
    {
        public DataSet addComponent(SqlParameter sqlParameter, string sqlConnectionString)
        {
            throw new NotImplementedException();
        }

        public DataSet getComponent(SqlParameter sqlParameter, string con)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = new SqlCommand("getComponent", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(sqlParameter);
                DataSet ds = new DataSet();
                try
                {
                    connection.Open();
                    SqlDataAdapter ad = new SqlDataAdapter();
                    ad.SelectCommand = cmd;
                    ad.Fill(ds);
                }
                catch (Exception e)
                {
                    Console.WriteLine("cant open connection" + e.Message);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
                return ds;
            }

        }
        public DataSet getComponentByNamingConvention(List<SqlParameter> stringParams, string con)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = new SqlCommand("getComponentsByNamingConvention", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddRange(stringParams.ToArray());     
                DataSet ds = new DataSet();
                try
                {
                    connection.Open();
                    SqlDataAdapter ad = new SqlDataAdapter();
                    ad.SelectCommand = cmd;
                    ad.Fill(ds);
                }
                catch (Exception e)
                {
                    Console.WriteLine("cant open connection" + e.Message);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
                return ds;
            }
           
        }
        public DataSet getAllComponents(string con)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = new SqlCommand("getAllComponents", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                try
                {
                    connection.Open();
                    SqlDataAdapter ad = new SqlDataAdapter();
                    ad.SelectCommand = cmd;
                    ad.Fill(ds);
                }
                catch (Exception e)
                {
                    Console.WriteLine("cant open connection" + e.Message);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
                return ds;
            }
        }




    }
}
