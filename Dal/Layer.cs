﻿using DalContracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;

namespace Dal
{
    [Implement(typeof(ILayer))]
    class Layer : ILayer
    {
        public DataSet getLayers(string con)
        {
            using (SqlConnection connection = new SqlConnection(con))
            {
                SqlCommand cmd = new SqlCommand("getLayers", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                try
                {
                    connection.Open();
                    SqlDataAdapter ad = new SqlDataAdapter();
                    ad.SelectCommand = cmd;
                    ad.Fill(ds);
                }
                catch (Exception e)
                {
                    Console.WriteLine("cant open connection" + e.Message);
                    throw;
                }
                finally
                {
                    connection.Close();
                }
                return ds;
            }
            
        }
    }
}
