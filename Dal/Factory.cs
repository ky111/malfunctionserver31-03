﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using DalContracts;

namespace Dal
{
    [Implement(typeof(IFactory))]
    class Factory : IFactory
    {
        public DataSet getIsOn(string sqlConnectionString)
        {
            using (SqlConnection con = new SqlConnection(sqlConnectionString))
            {
                SqlCommand cmd = new SqlCommand("getIsOn", con);
                cmd.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                try
                {
                    con.Open();
                    SqlDataAdapter ad = new SqlDataAdapter();//חוזר נתונים
                    ad.SelectCommand = cmd;
                    ad.Fill(ds);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    con.Close();
                }
                return ds;
            }
        }
    }
}
