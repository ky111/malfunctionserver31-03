﻿//using System;
//using System.Collections.Generic;
//using System.Text;

//namespace Dal
//{
//    class User
//    {
//    }
//}
using DalContracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;

namespace Dal
{
    [Implement(typeof(IUser))]
    class User : IUser
    {
        public DataSet checkUser(List<SqlParameter> listParameters, string sqlConnectionString)
        {
            using (SqlConnection con = new SqlConnection(sqlConnectionString))
            {
                SqlCommand cmd = new SqlCommand("checkUser", con);
                cmd.Parameters.AddRange(listParameters.ToArray());
                cmd.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                try
                {
                    con.Open();
                    SqlDataAdapter ad = new SqlDataAdapter();//חוזר נתונים
                    ad.SelectCommand = cmd;
                    ad.Fill(ds);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    con.Close();
                }
                return ds;
            }
        }

        public DataSet getResponsible(string sqlConnectionString)
        {
            using (SqlConnection con = new SqlConnection(sqlConnectionString))
            {
                SqlCommand cmd = new SqlCommand("getResponsible", con);
                cmd.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                try
                {
                    con.Open();
                    SqlDataAdapter ad = new SqlDataAdapter();//חוזר נתונים
                    ad.SelectCommand = cmd;
                    ad.Fill(ds);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    con.Close();
                }
                return ds;
            }
        }

        public DataSet insertUser(List<SqlParameter> listParameters, string sqlConnectionString)
        {
            using (SqlConnection con = new SqlConnection(sqlConnectionString))
            {
                SqlCommand cmd = new SqlCommand("addResponsible", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddRange(listParameters.ToArray());
                DataSet ds = new DataSet();

                try
                {
                    con.Open();
                    SqlDataAdapter ad = new SqlDataAdapter();
                    ad.SelectCommand = cmd;
                    ad.Fill(ds);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    con.Close();
                }
                return ds;
            }
        }
    }
}
