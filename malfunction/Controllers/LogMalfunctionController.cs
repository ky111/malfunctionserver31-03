﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace malfunction.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LogMalfunctionController : ControllerBase
    {
        string con;
        public LogMalfunctionController(string _con) { con = _con; }
        [HttpGet]
        [Route("{action}")]
        public ActionResult getLogmalfunctions()
        {
            var res = Factory.factory.getInstance().Resolve<DalContracts.ILogMalfunction>().getLogmalfunctions(con);
            var i = convert.convert.convertDBsetToDTO<DTO.ViewLogDTO>(res);
            return Ok(i);
        }
        [HttpGet]
        [Route("{action}")]
        public ActionResult getLogMalfunctionStatus()
        {
            var res = Factory.factory.getInstance().Resolve<DalContracts.ILogMalfunctionStatus>().getLogMalfunctionStatus(con);
            var res2 = convert.convert.convertDBsetToDTO<LogMalfunctionStatusDTO>(res);
            return Ok(res2);
        }




        [HttpPost]
        [Route("{action}")]

        public ActionResult addLogMalfunctionToDB([FromBody] DTO.AddLogMalfunctionDTO almDTO)
        {
            var param = convert.convert.convertDTOToSqlParameters(almDTO);         
            var user = Factory.factory.getInstance().Resolve<DalContracts.ILogMalfunction>().addLogMalfunctionToDB(param, con);
            var res = convert.convert.convertDBsetToDTO<int>(user);
            return Ok(res);
        }
    }
}
