﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DalContracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DTO;

namespace malfunction.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ComponentController : ControllerBase
    {
        string con;
        public ComponentController(string _con) { con = _con; }

        [HttpGet]
        [Route("{action}")]
        public ActionResult getComponent(int layerId)

        {
            var param = convert.convert.convertDTOToSqlParameters(layerId, "layerId");
            var res = Factory.factory.getInstance().Resolve<IComponent>().getComponent(param[0], con);
            var i = convert.convert.convertDBsetToDTO<ComponentDTO>(res);
            return Ok(i);
        }
        [HttpGet]
        [Route("{action}/{first}/{contains}")]
        public List<object> getComponentByNamingConvention(string first, string contains)
        {
            var stringParams = convert.convert.convertDTOToSqlParameters(first, "first");
            stringParams.Add(convert.convert.convertDTOToSqlParameters(contains, "contains")[0]);
            var res = Factory.factory.getInstance().Resolve<IComponent>().getComponentByNamingConvention(stringParams, con);
            List<object> i = convert.convert.convertDBsetToDTO<ComponentDTO>(res);
            //return Ok(i);
            return i;
        }

        public componentNode searchTree(componentNode element, string matchingTitle)
        {
            if (element.name == matchingTitle)
            {
                return element;
            }
            else if (element.children != null)
            {
                int i;
                componentNode result = null;
                for (i = 0; result == null && i < element.children.Count; i++)
                {
                    result = searchTree(element.children[i], matchingTitle);
                }
                return result;
            }
            return null;
        }

        [HttpGet]
        [Route("{action}/{componentName}")]
        public ActionResult getComponentsForTree(string componentName)
        {
            var res1 = Factory.factory.getInstance().Resolve<IHirarchia>().getHirarchia(con);
            var i2 = convert.convert.convertDBsetToDTO<HirarchiaDTO>(res1);
            //List<object> myList = HirarchiaController.getHirarchiaList();
            List<string> l = new List<string>();
            foreach (HirarchiaDTO item in i2)
            {
                l.Add(item.name);
            }
            string[] hirarchiaList = l.ToArray();
            // string[] hirarchiaList = { "cty", "stn", "flr", "com", "dir", "num" };
            List<componentNode> myTreeComponent = new List<componentNode>();
            List<ComponentDTO> myListComponent = new List<ComponentDTO>();
            var i = 0;
            var first = hirarchiaList[i];
            var contain = hirarchiaList[i];
            List<object> cityList = getComponentByNamingConvention(first, contain);
            foreach (ComponentDTO item in cityList)
            {
                myListComponent.Add(item);
            }
            foreach (ComponentDTO item in cityList)
            {
                var myNode = new componentNode()
                {
                    name = item.name,
                    showName = item.showName,
                    children = new List<componentNode>()
                };
                myTreeComponent.Add(myNode);
            }


            var x = componentName.IndexOf(hirarchiaList[i]);
            if (x == -1)
            {
                return Ok(-1);
            }
            var aba = componentName.Substring(x);
            componentName = componentName.Substring(0, x);
            var flag = true;

            while (componentName != "" && flag)
            {
                i++;
                first = hirarchiaList[i];
                List<object> d = getComponentByNamingConvention(first, aba);
                foreach (ComponentDTO item in d)
                {
                    myListComponent.Add(item);
                }
                if (d != null)
                {
                    var abaName = aba;
                    //.Substring(aba.IndexOf("_")+1);
                    var flagFind = true;
                    componentNode abaObject = new componentNode();
                    for (var index = 0; index < myTreeComponent.Count && flagFind; index++)
                    {
                        abaObject = searchTree(myTreeComponent[index], abaName);
                        if (abaObject != null)
                            flagFind = false;
                    }
                    foreach (ComponentDTO item in d)
                    {
                        var myNode = new componentNode()
                        {
                            name = item.name,
                            showName = item.showName
                        };
                        abaObject.children.Add(myNode);
                    }
                }
                else
                {
                    flag = false;
                }

                x = componentName.IndexOf(hirarchiaList[i]);
                var partName = componentName.Substring(x);
                aba = partName + aba;
                componentName = componentName.Substring(0, x);
            }

            var res = new doubleTreeDTO();
            res.components.AddRange(myListComponent);
            res.nodes.AddRange(myTreeComponent);

            return Ok(res);
        }
        [HttpGet]
        [Route("{action}")]
        public ActionResult GetAllComponents()
        {
            var ds = Factory.factory.getInstance().Resolve<IComponent>().getAllComponents(con);
            var res = convert.convert.convertDBsetToDTO<ComponentDTO>(ds);
            return Ok(res);
        }

    }
}


