﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DTO;
using DalContracts;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace malfunction.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LayerController : Controller
    {
        string con;
        public LayerController(string _con) { con = _con; }

        [HttpGet]
        [Route("{action}")]
        public ActionResult getLayers()

        {

            var res = Factory.factory.getInstance().Resolve<ILayer>().getLayers(con);
            var i = convert.convert.convertDBsetToDTO<LayerDTO>(res);
            return Ok(i);
        }
    }
}
