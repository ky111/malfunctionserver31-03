﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DalContracts;
using DTO;

namespace malfunction.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServiceCenterController : ControllerBase
    {
        public string con;
        public ServiceCenterController(string _con)
        {
            con = _con;
        }
        [HttpGet]
        [Route("{action}")]
        public List<object> getNewMalfunctions()
        {
            var resulteFromDB = Factory.factory.getInstance().Resolve<IMalfunction>().getNewMalfuctions(con);
            var resulte = convert.convert.convertDBsetToDTO<newMalfunctionsToServiceCenterDTO>(resulteFromDB);
            return resulte;
        }
    }
}
