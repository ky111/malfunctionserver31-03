﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DalContracts;

namespace malfunction.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FactoryController : ControllerBase
    {
        string con;
        public FactoryController(string _con)
        {
            con = _con;
        }

        [HttpGet]
        [Route("{action}")]
        public ActionResult getIsOn()
        {
            var res = Factory.factory.getInstance().Resolve<IFactory>().getIsOn(con);
            var res2 = convert.convert.convertDBsetToDTO<string>(res);
            return Ok(res2);
        }


    }
}