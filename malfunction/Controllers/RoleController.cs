﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace malfunction.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        string con;
        public RoleController(string _con) { con = _con; }
        [HttpGet]
        [Route("{action}")]
        public ActionResult getRoles()
        {
            var res = Factory.factory.getInstance().Resolve<DalContracts.IRole>().getRoles(con);
            var res2 = convert.convert.convertDBsetToDTO<RolesDTO>(res);
            return Ok(res2);
        }
    }
}