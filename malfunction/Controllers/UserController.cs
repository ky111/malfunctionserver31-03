﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DTO;
using DalContracts;
using System.Data.SqlClient;

namespace malfunction.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {

        string con;
        public UserController(string _con) { con = _con; }
        [HttpGet]
        [Route("{action}")]
        public ActionResult getResponsibles()
        {
            var res = Factory.factory.getInstance().Resolve<DalContracts.IUser>().getResponsible(con);
            var res2 = convert.convert.convertDBsetToDTO<ResponsibleShowDTO>(res);
            return Ok(res2);
        }


        [HttpPost]
        [Route("{action}")]

        public ActionResult checkUser([FromBody] DTO.UserDTO userDTO)
        {
            var l = convert.convert.convertDTOToSqlParameters(userDTO.name, "name");
            var l2 = convert.convert.convertDTOToSqlParameters(userDTO.responsiblePassword, "responsiblePassword");
            List<SqlParameter> sqlsList = new List<SqlParameter>();
            sqlsList.Add(l[0]);
            sqlsList.Add(l2[0]);
            var user = Factory.factory.getInstance().Resolve<DalContracts.IUser>().checkUser(sqlsList, con);
            var res = convert.convert.convertDBsetToDTO<UserDTO>(user);
            return Ok(res);
        }

        [HttpPost]
        [Route("{action}")]
        public ActionResult PostInsertaddUserToDB([FromBody] DTO.ResponsibleDTO responsibleDTO)
        {
            var l = convert.convert.convertDTOToSqlParameters(responsibleDTO);

            var user = Factory.factory.getInstance().Resolve<DalContracts.IUser>() as DalContracts.IUser;
            user.insertUser(l, con);
            /*var k = convert.convert.convertDBsetToDTO<int>(c);*///=conver של יעל
            return Ok();
        }
    }
}