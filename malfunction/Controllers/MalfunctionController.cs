﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DTO;
using DalContracts;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace malfunction.Controllers
{


    [Route("api/[controller]")]
    public class MalfunctionController : Controller
    {
        string con;
        public MalfunctionController(string _con) {con = _con;}

        [HttpGet]
        [Route("{action}")]
        public ActionResult getMalfunctionStatus()
        {
            var res = Factory.factory.getInstance().Resolve<DalContracts.IMalfunctionStatus>().getMalfunctionStatus(con);
            var res2 = convert.convert.convertDBsetToDTO<MalfunctionStatusDTO>(res);
            return Ok(res2);
        }

        [HttpGet]
        [Route("{action}")]
        public ActionResult getMalfunctions()
        {
            var res = Factory.factory.getInstance().Resolve<DalContracts.IMalfunction>().getMalfunctions(con);
            var res2 = convert.convert.convertDBsetToDTO<MalfunctionByComponentId>(res);
            return Ok(res2);
        }
        [HttpPost]
        [Route("{action}")]
        public ActionResult addMalfunctionToDB([FromBody] AddMalfunctionDTO mDTO)
        { 
            var param = convert.convert.convertDTOToSqlParameters(mDTO);
            var i = Factory.factory.getInstance().Resolve<IMalfunction>().addMalfunction(param, con);
            var res = convert.convert.convertDBsetToDTO<int>(i);
            return Ok(res);
        }
        [HttpGet]
        [Route("{action}/{componentId}")]
        public ActionResult getMalfunctionByComponentId(int componentId)
        { 
            var param = convert.convert.convertDTOToSqlParameters(componentId,"componentId");
            var i = Factory.factory.getInstance().Resolve<IMalfunction>().getMalfunctionByComponentId(param, con);
            var res = convert.convert.convertDBsetToDTO<MalfunctionByComponentId>(i);
            return Ok(res);
        }
    }
}
