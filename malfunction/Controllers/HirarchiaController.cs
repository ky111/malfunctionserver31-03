﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DalContracts;
using DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace malfunction.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HirarchiaController : ControllerBase
    {
        static string con;
        
        public HirarchiaController(string _con) { con = _con; }

        [HttpGet]
        [Route("{action}")]
        public static List<object> getHirarchiaList()
        {
            var res = Factory.factory.getInstance().Resolve<IHirarchia>().getHirarchia(con);
            var i = convert.convert.convertDBsetToDTO<HirarchiaDTO>(res);
            return i;
        }

    }
}